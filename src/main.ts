import { useVncStore } from './store/vnc';
import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import AppLayout from '@/components/layout/AppLayout.vue';
import EmptyLayout from '@/components/layout/EmptyLayout.vue';
import AxiosConfig from './api/AxiosConfig';
import GeneralEndpoint from './api/GeneralEndpoint';
import { useAuthStore } from './store/auth';
import { store }  from './store/store';

const authStore = useAuthStore(store);
const vncStore = useVncStore(store);

const startApp = () => {
    const app = createApp(App);

    app.use(store);
    app.use(router);
    app.component('default-layout', AppLayout);
    app.component('empty-layout', EmptyLayout);

    app.mount('#app');
};

AxiosConfig.setupAxios();

GeneralEndpoint.getInit().then(response => {
    authStore.setAuthRequired(response.status !== 200 || response.data.authRequired);
    authStore.setIsAdoptable(response.status === 200 && response.data.canAdopt);
    if(response.status === 200) {
        vncStore.setInitialResolution({ width: response.data.initialWidth,  height: response.data.initialHeight });
    }
    startApp();
}).catch(() => {
    authStore.setIsAdoptable(false);
    startApp();
});

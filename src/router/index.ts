import { useAuthStore } from './../store/auth';
import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import SettingsView from '../views/settings/SettingsView.vue';
import LoginView from '../views/LoginView.vue';
import AdoptView from '../views/AdoptView.vue';
import ProfileSettings from '../views/settings/profile/ProfileSettings.vue';
import ActivationSettings from '../views/settings/ActivationSettings.vue';
import GeneralSettings from '@/views/settings/general/GeneralSettings.vue';
import { store } from '../store/store';
import { clearQueue } from '@/components/dialogs/Dialogs';

const authStore = useAuthStore(store);

declare module 'vue-router' {
  interface RouteMeta {
    layout?: string,
    requiresAuth: boolean
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsView,
      children: [
        {
          path: 'general',
          name: 'settings-general',
          component: GeneralSettings
        },
        {
          path: 'profile',
          name: 'settings-profile',
          component: ProfileSettings
        }, 
        {
          path: 'activation',
          name: 'settings-activation',
          component: ActivationSettings
        }
      ],
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        layout: 'empty',
        requiresAuth: false
      }
    },
    {
      path: '/adopt',
      name: 'adopt',
      component: AdoptView,
      meta: {
        layout: 'empty',
        requiresAuth: false
      }
    }
  ]
});

router.beforeEach((to, _from) => {
  // Clear any Dialogs in queue
  clearQueue();

  // Force Adopt View if is adoptable and auth is enabled
  if(authStore.authRequired && authStore.adoptable === true && to.name !== 'adopt') {
    return {
      name: 'adopt'
    };
  }

  // Go Home instead of adopt if is already adopted and navigating to adopt view
  if(authStore.adoptable === false && to.name === 'adopt') {
    return {
      name: 'home'
    };
  }

  // Go Home instead of login if authentication is disabled
  if(authStore.authRequired === false && to.name === 'login') {
    return {
      name: 'home'
    };
  }

  // Go Home instead of login if already logged in
  if(authStore.isLoggedIn && to.name === 'login') {
    return {
      name: 'home'
    };
  }

  if(authStore.authRequired && to.meta.requiresAuth && !authStore.isLoggedIn) {
    return {
      name: 'login'
    };
  }
});

export default router;

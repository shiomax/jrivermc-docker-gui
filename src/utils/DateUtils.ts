
const toDoubleDigitString = (input: number) => 
    input <= 9 ? `0${input}` : input.toString();

const formatDateTime = (input: string | Date | null | undefined): string => {
    if(!input) return '-';

    const dt = input instanceof Date ? input : new Date(input);

    if(!dt) return '-';

    const year = dt.getFullYear();
    const month = toDoubleDigitString(dt.getMonth() + 1);
    const day = toDoubleDigitString(dt.getDate());
    const hour = toDoubleDigitString(dt.getHours());
    const minute = toDoubleDigitString(dt.getMinutes());
    const seconds = toDoubleDigitString(dt.getSeconds());

    return `${year}-${month}-${day} ${hour}:${minute}:${seconds}`;
};

export { formatDateTime };
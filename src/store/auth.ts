import { defineStore } from 'pinia';
import router from '../router';

const KEY_TOKEN = "JWT_TOKEN";
const KEY_REFRESH_TOKEN = "REFRESH_TOKEN";

enum UserRole {
    Owner = 1,
    Admin = 2,
    Member = 3
}

interface TokenPayload {
    sub: string,
    jti: string,
    id: number,
    nbf: number,
    exp: number,
    iat: number,
    role: UserRole
}

export const useAuthStore = defineStore('auth', {
    state: () => {
        const initialToken = localStorage.getItem(KEY_TOKEN) ?? null;
        const initialRefreshToken = localStorage.getItem(KEY_REFRESH_TOKEN) ?? null;

        let initialPayload: TokenPayload | null;
        try {
            initialPayload = initialToken ? JSON.parse(atob(initialToken.split('.')[1])) as TokenPayload : null;
        } catch {
            initialPayload = null;
        }

        let isLoggedIn = false;
        if(initialPayload && initialRefreshToken && initialPayload)
            isLoggedIn = true;

        const initialState = {
            refreshToken: initialRefreshToken as string | null,
            token: initialToken as string | null,
            tokenPayload: initialPayload as TokenPayload | null,
            adoptable: false,
            authRequired: true,
            loggedIn: isLoggedIn
        };

        return initialState;
    },

    getters: {
        isLoggedIn(state): boolean {
            // Authentication is not required don´t check
            if(!state.authRequired) return true;
            if(!state.loggedIn || !state.token || !state.tokenPayload || !state.refreshToken) return false;
            return true;
        }
    },

    actions: {
        setJwt(newToken: string, newRefreshToken: string): void {
            this.token = newToken;
            this.refreshToken = newRefreshToken;
            this.loggedIn = true;
            if(!this.token) this.tokenPayload = null;
            else this.tokenPayload = JSON.parse(atob(this.token.split('.')[1])) as TokenPayload;
            
            // Store in local Storage
            if(this.token) localStorage.setItem(KEY_TOKEN, this.token);
            else localStorage.removeItem(KEY_TOKEN);

            if(this.refreshToken) localStorage.setItem(KEY_REFRESH_TOKEN, this.refreshToken);
            else localStorage.removeItem(KEY_REFRESH_TOKEN);
        },
        setIsAdoptable(newValue: boolean): void {
            this.adoptable = newValue;
        },
        setAuthRequired(newValue: boolean): void {
            this.authRequired = newValue;
            if(newValue === false) {
                localStorage.removeItem(KEY_TOKEN);
                localStorage.removeItem(KEY_REFRESH_TOKEN);
            }
        },
        logout(): void {
            localStorage.removeItem(KEY_REFRESH_TOKEN);
            localStorage.removeItem(KEY_TOKEN);
            this.$reset();
            router.push({ name: 'login', replace: true });
        },
        isTokenExpired(): boolean {
            if(!this.tokenPayload || ! this.tokenPayload.exp) return true;
            return Date.now() >= (this.tokenPayload.exp * 1000);
        }
    }
});
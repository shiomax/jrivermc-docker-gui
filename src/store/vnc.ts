import { defineStore } from 'pinia';

export interface VNCResolution {
    width: number,
    height: number
}

export type Level = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

export function isLevel(level: unknown): level is Level {
    if(level === null || level === undefined) return false;
    if(typeof level !== 'number') return false;
    return level as number >= 1 && level as number <= 9;
}

const KEY_RESOLUTION = "VNC_RESOLUTION";
const KEY_COMPRESSION_LEVEL = "VNC_COMPRESSION_LEVEL";
const KEY_QUALITY = "VNC_QUALITY";

const initVNCResolution = function(): VNCResolution | null {
    const rawValue = localStorage.getItem(KEY_RESOLUTION);
    if(!rawValue) return null;

    let parsed;
    try {
        parsed = JSON.parse(rawValue) as VNCResolution;
    } catch {
        return null;
    }

    if(!parsed || typeof parsed.height !== 'number' || typeof parsed.width !== 'number') {
        return null;
    }

    return parsed;
};

const initLevel = function(key: string) : Level | null {
    const rawValue = localStorage.getItem(key);
    if(rawValue === null || rawValue === undefined || rawValue == '') return null;

    let parsed;
    try {
        parsed = JSON.parse(rawValue);
    } catch {
        return null;
    }

    if(!isLevel(parsed)) {
        return null;
    }

    return parsed;
};

export const useVncStore = defineStore('vnc', {
    state: () => {
        return {
            resolution: initVNCResolution(),
            defaultResolution: { height: 768, width: 1280 } as VNCResolution,
            quality: initLevel(KEY_QUALITY),
            compressionLevel: initLevel(KEY_COMPRESSION_LEVEL)
        };
    },

    actions: {
        setResolution: function(newResolution: VNCResolution | null) {
            if(!newResolution) {
                localStorage.removeItem(KEY_RESOLUTION);
                this.resolution = null;
                return;
            }
            localStorage.setItem(KEY_RESOLUTION, JSON.stringify(newResolution));
            this.resolution = newResolution;
        },
        setInitialResolution: function(resolution: VNCResolution) {
            this.defaultResolution.height = resolution.height;
            this.defaultResolution.width = resolution.width;
        },
        setQuality: function(newQuality: Level | null) {
            this.quality = newQuality;
            if(newQuality === null) {
                localStorage.removeItem(KEY_QUALITY);
                return;
            }
            localStorage.setItem(KEY_QUALITY, JSON.stringify(newQuality));
        },
        setCompressionLevel: function(newCompressionLevel: Level | null) {
            this.compressionLevel = newCompressionLevel;
            if(newCompressionLevel === null) {
                localStorage.removeItem(KEY_COMPRESSION_LEVEL);
                return;
            }
            localStorage.setItem(KEY_COMPRESSION_LEVEL, JSON.stringify(newCompressionLevel));
        }
    }
});
interface Dialog {
    title: string,
    message: string,
    type: 'Alert' | 'Confirm'
}

interface Alert {
    title?: string,
    message: string
}

interface Confirm {
    title?: string,
    message: string
}

interface Subscriber {
    notifyNewDialog: () => void
}

interface QueueItem {
    dialog: Dialog,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    resolve: any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    reject: any
}

const queue = new Array<QueueItem>();
const subs = new Array<Subscriber>();

const showAlert = async (alert: Alert): Promise<void> => {
    const dialog: Dialog = {
        title: alert.title ?? 'Alert',
        message: alert.message,
        type: 'Alert'
    };

    try {
        await new Promise((resolve, reject) => {
            queue.push({ dialog, resolve, reject });
            for(const sub of subs) {
                sub.notifyNewDialog();
            }
        });
    } finally {
        const index = queue.findIndex(x => x.dialog == dialog);
        if(index >= 0) {
            queue.splice(index, 1);
        }
    }
};

const showConfirm = async (confirm: Confirm): Promise<boolean> => {
    const dialog: Dialog = {
        title: confirm.title ?? 'Confirm',
        message: confirm.message,
        type: 'Confirm'
    };

    try {
        const result = await new Promise((resolve, reject) => {
            queue.push({ dialog, resolve, reject });
            for(const sub of subs) {
                sub.notifyNewDialog();
            }
        });

        if(typeof result === 'boolean') {
            return result; 
        }
        throw `Invalid result: ${result}`;
    } finally {
        const index = queue.findIndex(x => x.dialog == dialog);
        if(index >= 0) {
            queue.splice(index, 1);
        }
    }
};

const clearQueue = () => {
    while(queue.length) {
        const item = queue.pop();
        item?.reject();
    }
};

const subscribe = (s: Subscriber) => {
    subs.push(s);
};

const unsubscribe = (s: Subscriber) => {
    const index = subs.indexOf(s);
    if(index < 0) return;
    subs.splice(index, 1);
};

export type { Alert, Confirm, QueueItem, Subscriber };
export { showAlert, showConfirm, queue, clearQueue, subscribe, unsubscribe };
import axios from "axios";

interface LoginRequest {
    username: string,
    password: string,
    twoFactorCode: string | null
}

interface LoginResponse {
    success: boolean,
    token: string | null,
    refreshToken: string | null,
    errors: Array<string>,
    twoFactorRequired: boolean
}

const login = async function(request: LoginRequest) {
    const response = await axios.post<LoginResponse>('auth/login', request);
    return response;
};

interface AdoptRequest {
    username: string,
    password: string
}

interface AdoptResponse {
    success: boolean,
    errors: Array<string>
}

const adopt = async function(request: AdoptRequest) {
    const response = await axios.post<AdoptResponse>('auth/adopt', request);
    return response;
};

interface ChangeUsernameRequest {
    password: string,
    newUsername: string
}

interface ChangeUsernameResponse {
    success: boolean,
    errors: Array<string>
}

const changeUsername = async function(request: ChangeUsernameRequest) {
    const response = await axios.post<ChangeUsernameResponse>('auth/changeUsername', request);
    return response;
};

interface ChangePasswordRequest {
    password: string,
    newPassword: string
}

interface  ChangePasswordResponse {
    success: boolean,
    errors: Array<string>
}

const changePassword = async function(request: ChangePasswordRequest) {
    const response = await axios.post<ChangePasswordResponse>('auth/changePassword', request);
    return response;
};

interface RefreshRequest {
    token: string,
    refreshToken: string
}

interface RefreshResponse {
    success: boolean,
    errors: Array<string>,
    token: string | null,
    refreshToken: string | null
}

const refreshToken = async function(request: RefreshRequest) {
    const response = await axios.post<RefreshResponse>('auth/refresh', request);
    return response;
};

interface LogoutRequest {
    token: string,
    refreshToken: string
}

interface LogoutResponse {
    success: boolean,
    errors: Array<string>
}

const logout = async function(request: LogoutRequest) {
    const response = await axios.post<LogoutResponse>('auth/logout', request);
    return response;
};

interface GetUsersResponse {
    users: Array<GetusersResponseItem>
}

interface GetusersResponseItem {
    id: number,
    username: string,
    twoFactor: boolean,
    role: 'Owner' | 'Admin' | 'Member'
}

const getUsers = async function() {
    const response = await axios.get<GetUsersResponse>('auth/users');
    return response;
};

const deleteUser = async function(id: string) {
    const response = await axios.delete(`auth/users/${id}`);
    return response;
};

interface CreateUserRequest {
    username: string,
    password: string,
    role: 'Admin' | 'Member'
}

interface CreateUserResponse {
    success: boolean,
    errors: Array<string>
}

const createUser = async function(request: CreateUserRequest) {
    const response = await axios.post<CreateUserResponse>('auth/createUser', request);
    return response; 
};

interface GetTwoFactorResponse {
    factors: Array<GetTwoFactorResponseItem>
}

interface GetTwoFactorResponseItem {
    id: number,
    twoFactorMethod: 'Totp',
    name: string,
    createdAt: Date
}

const getTwoFactors = async function() {
    const response = await axios.get<GetTwoFactorResponse>('auth/twofactor');
    return response;
};

interface CreateTwoFactorRequest {
    name: string
}

interface CreateTwoFactorResponse {
    success: boolean,
    errors: Array<string>,
    secretBase32: string,
    totpKeyUri: string
}

const createTwoFactor = async function(request: CreateTwoFactorRequest) {
    const response = await axios.post<CreateTwoFactorResponse>('auth/twofactor', request);
    return response;
};

interface GetTwoFactorEnabledResponse {
    enabledGlobal: boolean,
    enabled: boolean
}

const getTwofactorEnabled = async function() {
    const response = await axios.get<GetTwoFactorEnabledResponse>('auth/twofactor/enabled');
    return response;
};

interface PostTwoFactorEnabledRequest {
    enabled: boolean
}

interface PostTwoFactorEnabledResponse {
    success: boolean,
    errors: Array<string>
}

const postTwoFactorEnabled = async function(request: PostTwoFactorEnabledRequest) {
    const response = await axios.post<PostTwoFactorEnabledResponse>('auth/twofactor/enabled', request);
    return response;
};

interface DeleteTwoFactorResponse {
    success: boolean,
    errors: Array<string>
}

const deleteTwoFactor = async function(id: number) {
    const response = await axios.delete<DeleteTwoFactorResponse>(`auth/twofactor/${id}`);
    return response;
};

export type {
    GetTwoFactorResponseItem
};

export default {
    login,
    logout,
    adopt,
    changeUsername,
    changePassword,
    refreshToken,
    getUsers,
    deleteUser,
    createUser,
    getTwoFactors,
    createTwoFactor,
    getTwofactorEnabled,
    postTwoFactorEnabled,
    deleteTwoFactor
};
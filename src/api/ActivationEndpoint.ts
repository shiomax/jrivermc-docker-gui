import type { AxiosRequestConfig } from "axios";
import axios from "axios";

interface PostActivateMjrFileResponse {
    success: boolean,
    errors: Array<string> | null
}

const activateMjr = async function(file: File) {
    const formData = new FormData();
    formData.append('key.mjr', file);

    const requestConfig: AxiosRequestConfig = {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    };

    const response = await axios.post<PostActivateMjrFileResponse>('activation/mjr', formData, requestConfig);
    return response;
};

interface GetActivateMjrStatusResponse {
    isRunning: boolean,
    output: Array<string>,
    exitCode: number | null,
    success: boolean,
    errors: Array<string> | null
}

const activateMjrStatus = async function() {
    const response = await axios.get<GetActivateMjrStatusResponse>('activation/mjr/status');
    return response;
};

export default {
    activateMjr,
    activateMjrStatus
};
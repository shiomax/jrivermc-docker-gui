import { useAuthStore } from './../store/auth';
import axios, { type AxiosError, AxiosHeaders } from "axios";
import { watchEffect } from "vue";
import authEndpoint from "./AuthEndpoint";
import { store } from '@/store/store';

const authStore = useAuthStore(store);

declare module 'axios' {
    export interface AxiosRequestConfig {
        _retry?: boolean | undefined,
        _queued?: boolean | undefined
    }
}

interface PromiseCallbacks {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    resolve: any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    reject: any
}

class TokenRefreshInterceptor {

    failedQueue: Array<PromiseCallbacks>;
    isRefreshing: boolean;

    constructor() {
        this.failedQueue = new Array<PromiseCallbacks>();
        this.isRefreshing = false;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    rejectQueue(err: any) {
        while(this.failedQueue.length > 0) {
            const cur = this.failedQueue.pop();
            cur?.reject(err);
        }
    }

    retryQueue(token: string) {
        while(this.failedQueue.length > 0) {
            const cur = this.failedQueue.pop();
            cur?.resolve(token);
        }
    }

    onRejected(error: AxiosError) {

        if(error.response?.status !== 401) return Promise.reject(error);

        // Skip if authentication is disabled
        if(authStore.authRequired === false) return Promise.reject(error);

        const originalRequest = error.config;

        if(!originalRequest) return Promise.reject(error);
        if(originalRequest._retry) return Promise.reject(error);
        originalRequest._retry = true;

        if(this.isRefreshing) {
            return new Promise((resolve, reject) => {
                this.failedQueue.push({ resolve, reject });
            }).then(token => {
                originalRequest._queued = true;
                if(!originalRequest.headers) originalRequest.headers = new AxiosHeaders({});
                originalRequest.headers['Authorization'] = `Bearer ${token}`;
                return axios(originalRequest);
            }).catch(err => {
                return Promise.reject(err);
            });
        }
        
        const token = authStore.token;
        const refreshToken = authStore.refreshToken;
        if(!token || !refreshToken) return Promise.reject(error);
        
        this.isRefreshing = true;

        return new Promise((resolve, reject) => {
            authEndpoint.refreshToken({ token, refreshToken }).then(response => {
                if(response.status !== 200 || !response.data.success || !response.data.token || !response.data.refreshToken) {
                    // check again in case token was refreshed by something else
                    const storeToken = authStore.token;
                    if(authStore.isTokenExpired() || !storeToken) {
                        this.rejectQueue(error);
                        reject(error);
                        authStore.logout();
                    } else {
                        this.retryQueue(storeToken);
                        if(!originalRequest.headers) originalRequest.headers = new AxiosHeaders({});
                        originalRequest.headers['Authorization'] = `Bearer ${storeToken}`;
                        resolve(axios(originalRequest));
                    }
                } else {
                    authStore.setJwt(response.data.token, response.data.refreshToken);
                    this.retryQueue(response.data.token);
                    if(!originalRequest.headers) originalRequest.headers = new AxiosHeaders({});
                    originalRequest.headers['Authorization'] = `Bearer ${response.data.token}`;
                    resolve(axios(originalRequest));
                }
            }).catch(err => {
                this.rejectQueue(err);
                reject(err);
                authStore.logout();
            }).finally(() => this.isRefreshing = false);
        });
    }
}

const setupAxios = function() {
    axios.defaults.baseURL = import.meta.env.VITE_API_URL ?? 
        `${window.location.protocol}//${window.location.host}/api/`;

    watchEffect(() => {
        if(!authStore.authRequired) {
            if(axios.defaults.headers.common['Authorization'])
                delete axios.defaults.headers.common['Authorization'];
        } else {
            const authToken = authStore.token;
            if(authToken)
                axios.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
            else if(axios.defaults.headers.common['Authorization'])
                delete axios.defaults.headers.common['Authorization'];
        }
    });

    const tokenRefreshInterceptor = new TokenRefreshInterceptor();
    axios.interceptors.response.use(response => response, (axiosError) => tokenRefreshInterceptor.onRejected(axiosError));
};

export default {
    setupAxios
};
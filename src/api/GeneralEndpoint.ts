import axios from "axios";

interface GetInitResponse {
    authRequired: boolean,
    canAdopt: boolean,
    initialWidth: number,
    initialHeight: number
}

const getInit = async function() {
    const response = await axios.get<GetInitResponse>('general/init');
    return response;
};

interface GetVersionResponse {
    imageVersion: string,
    debUrl: string,
    usesRepository: boolean,
    mediaCenterTag: string
}

const getVersion = async function() {
    const response = await axios.get<GetVersionResponse>('general/version');
    return response;
};

export type {
    GetVersionResponse,
    GetInitResponse
};

export default {
    getInit,
    getVersion
};
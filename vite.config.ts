import { fileURLToPath, URL } from 'url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import postcssNesting from 'postcss-nesting';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [ vue(), vueJsx() ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    postcss: {
      plugins: [
        postcssNesting
      ]
    }
  },
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:5800/',
        changeOrigin: true,
        secure: false,
        ws: false
      },
      '/websockify': {
        target: 'ws://localhost:5800/',
        changeOrigin: true,
        secure: false,
        ws: true
      }
    }
  }
});

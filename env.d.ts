/// <reference types="vite/client" />

interface ImportMetaEnv {
    readonly VITE_APP_NAME: string,
    readonly VITE_VNC_URL: string | null,
    readonly VITE_API_URL: string | null
}